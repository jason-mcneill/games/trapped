﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playCaptionAudio : MonoBehaviour
{
    public GameObject caption; //Drag n drop caption to play
    public GameObject audioClip;

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player"))
            caption.SetActive(true);
            audioClip.SetActive(true);
            gameObject.SetActive(false); //prevent the collider occuring again 
    }
}
