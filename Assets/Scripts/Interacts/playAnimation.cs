﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playAnimation : MonoBehaviour
{

    [SerializeField] private Animator myAnimationController;
    public GameObject doorSound;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        myAnimationController.SetBool("playerEnter", true);
        doorSound.SetActive(true);
        doorSound.SetActive(false);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            myAnimationController.SetBool("playerEnter", false);
    }
}
