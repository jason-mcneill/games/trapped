﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paperInteract : MonoBehaviour
{
    public GameObject MessagePanel;
    public GameObject Message;
    public GameObject Paper;
    public GameObject ScaryMessage;
    public GameObject AudioToStop;

    private bool pickUpAllowed;
    private bool paperOpened;

    private void Start()
    {
        MessagePanel.SetActive(false); //Set message to pickup item to false
    }

    private void Update()
    {
        if (pickUpAllowed && Input.GetKeyDown(KeyCode.F))
        {
            PickUp();
            paperOpened = true;
        }

        //Only works if the paper is open
        if (paperOpened && Input.GetKeyDown(KeyCode.Escape))
        {
            Message.SetActive(false); //Stop player seeing paper
            Destroy(gameObject); //Destroys game object the script is assigned to. We need this update to keep going until escape is clicked.
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name.Equals("Player"))
        {
            MessagePanel.SetActive(true); //Set message to pickup item to true
            pickUpAllowed = true;
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name.Equals("Player"))
        {
            MessagePanel.SetActive(false); //Set message to pickup item to false
            pickUpAllowed = false;
        }
    }

    private void PickUp()
    {
        Destroy(Paper); //Destroy the paper
        MessagePanel.SetActive(false); //Set message to pickup item to false
        Message.SetActive(true); //Enable the paper message
        Destroy(AudioToStop);
        ScaryMessage.SetActive(true);
    }
}
