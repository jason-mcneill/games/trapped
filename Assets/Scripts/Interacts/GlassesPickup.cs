﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using DG.Tweening;

public class GlassesPickup : MonoBehaviour
{

    //objects
    public GameObject MessagePanel;
    public GameObject Glasses;
    public GameObject EyeDissapearCollider;
    public GameObject EyesCollider;
    public GameObject Barrier;

    //captions
    public GameObject caption;

    private bool pickUpAllowed;

    //Post processing settings
    DepthOfField doflayer = null;
    public GameObject cameraObj; //For getting post process info from camera object
    public float aperatureVal = 10f;

    private void Start()
    {
        MessagePanel.SetActive(false); //Set message to pickup item to false
        
    }

    private void Update()
    {
        if (pickUpAllowed && Input.GetKeyDown(KeyCode.F))
        {
            PickUp();
            GlassesPickupEffects(true); //All the visuals
            caption.SetActive(true);
            EyesCollider.SetActive(true);
            EyeDissapearCollider.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name.Equals("Player"))
        {
            MessagePanel.SetActive(true); //Set message to pickup item to true
            pickUpAllowed = true;
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name.Equals("Player"))
        {
            MessagePanel.SetActive(false); //Set message to pickup item to false
            pickUpAllowed = false;
        }
    }

    private void PickUp()
    {
        Destroy(Glasses); //Destroys game object the script is assigned to
        Destroy(gameObject); //We also want to destroy the game object, otherwise the [Click F to pickup] continues to appear
        MessagePanel.SetActive(false); //Set message to pickup item to false
        Destroy(Barrier); //Destroy the barrier at the beginning
    }

    //tween settings
    [SerializeField] float maxAperature = 10f;
    [SerializeField] float duration = 0.5f;
    float minAperature = 0f; //This is the minimum value for the aperature 
    Tween dofTween;

    private void GlassesPickupEffects(bool value)
    {
        //This tween sequence goes from the minimum value set to the maxium
        dofTween = DOTween.To(() => minAperature,
            x => minAperature = x, value ? maxAperature : 0, duration).OnUpdate (UpdateDOFAnimation); //Sends the changed value for Ap through to the post processor
        dofTween.OnComplete(OnDOFAnimationEnd);
    }

    private void UpdateDOFAnimation ()
    {
        //dof setting changes
        PostProcessVolume volume = cameraObj.GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings(out doflayer);
        doflayer.aperture.value = minAperature; //Assign the layer the current aperature value after every update
    }

    private void OnDOFAnimationEnd ()
    {
        dofTween = null; //Sets tween to null so doesn't continue
    }

}
