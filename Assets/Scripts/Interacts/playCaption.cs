﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playCaption : MonoBehaviour
{

    public GameObject caption; //Drag n drop caption to play

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            caption.SetActive(true);
            gameObject.SetActive(false); //prevent the collider occuring again
    }
}
