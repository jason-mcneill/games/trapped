﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eyeAppear : MonoBehaviour
{
    public GameObject caption; //Drag n drop caption to play
    public GameObject audioClip;
    public GameObject eye;

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player"))
            caption.SetActive(true);
        audioClip.SetActive(true);
        gameObject.SetActive(false); //prevent the collider occuring again 
        eye.SetActive(true);
    }
}
