﻿using UnityEngine;


public class Interact : MonoBehaviour
{

    public GameObject MessagePanel;

    private bool pickUpAllowed;

    private void Start()
    {
        MessagePanel.SetActive(false); //Set message to pickup item to false
    }

    private void Update()
    {
        if (pickUpAllowed && Input.GetKeyDown(KeyCode.F))
        {
            PickUp();
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name.Equals("Player"))
        {
            MessagePanel.SetActive(true); //Set message to pickup item to true
            pickUpAllowed = true;
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name.Equals("Player"))
        {
            MessagePanel.SetActive(false); //Set message to pickup item to false
            pickUpAllowed = false;
        }
    }

    private void PickUp()
    {
        Destroy(gameObject); //Destroys game object the script is assigned to
        MessagePanel.SetActive(false); //Set message to pickup item to false
    }

}
