﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disappear : MonoBehaviour
{
    public GameObject givenObject;

    private void OnTriggerEnter(Collider other)
    {
        Destroy(givenObject); //Destroy said object
    }

}
