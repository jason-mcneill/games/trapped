﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndAudio : MonoBehaviour
{
    void OnEnd()
    {
        //When clip ends
        gameObject.SetActive(false);
    }
}
