﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudio : MonoBehaviour
{

    public GameObject AudioClip;

    void PlayAudioClip()
    {
        AudioClip.SetActive(true);
    }
}
