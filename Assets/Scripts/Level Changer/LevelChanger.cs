﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    public Animator animator;
    private int levelToLoad;

    // Update is called once per frame
    void Update()
    {
        //Use if you want to skip intro
        //if (Input.GetMouseButtonDown(0))
        //{
        //    FadeToLevel(1);
        //}
    }

    public void FadeToLevel (int NextLevel)
    {
        animator.SetTrigger("FadeOut");
        OnFadeComplete(NextLevel);
    }

    public void OnFadeComplete(int levelToLoad)
    {
        SceneManager.LoadScene(levelToLoad);
     }
}
